/**
 * Created by daniel on 29.04.2016.
 */

(function () {

    var DEFAULT_GUIDED_CHARACTER = "?";

    var selectors = {
        CONTENT_LEFT: "#contentLeft",
        CONTENT_CENTER: "#contentCenter",
        CONTENT_RIGHT: "#contentRight",

        DB_STRUCTURE: "#gqeDbStructure",
        QUERY_INFO: "#gqeQueryInfo",

        CURRENT_QUERY: "tabView:resultsForm:gqeQuery",
        RESULTS: "#gqeResults",
        RESULTS_BODY: "#resultsBody",
        QUERY_RESULTS: "#gqeQueryResults",
        RESULTS_FORM: "tabView:resultsForm",
        EDITOR_CONTAINER: "#gqeEditorContainer",
        EDITOR: "#gqeEditor",
        SAVE_QUERY_BUTTON: "#saveQuery",
        HIDE_RESULTS_BUTTON: "#hideResults",

        RIGHT_COMPONENT: "",
        HORIZONTAL_TOGGLER: "#horizontal_toggler",

        DATATABLE_ZOOM_DIALOG: "ZoomDialog",
        DATATABLE_ZOOM: "#tableContent",


        GUIDED_DIALOG: {
            DIALOG: "guidedDialog",
            EDITOR: "#guidedQuery",
            TABLE_BODY: "#tableBody",
            GQ_NAME: "#gqName",
            PREVIEW_CALL: "#previewCall",
        }
    };

    var dashboardState = {
        hiddenRightContent: false,
        hiddenResultsContent: false,
        ace_theme: "ace/theme/sqlserver",
        ace_editor_mode: "ace/mode/pgsql",
    };

    var gqe = {
        init: init,
        initDialog: initGuidedDialog,
        disposeDialog: disposeGuidedDialog,
        initZoomDialog: initZoomDialog,
        disposeZoomDialog: disposeZoomDialog,
        toggleResultsComponent: toggleResultsComponent,
        toggleGuidedQueryDialog: toggleGuidedQueryDialog,
        toggleZoomDialog: toggleZoomDialog,
        handleMessage: handleMessage,
        removeGuidedParameter: removeParameter,
        reloadQueryParameters: reparseQuery,

        saveQuery : saveQueryProcess,
        checkForGuided : checkForGuided,
        updateQuery : updateQueryInEditor,

        editor: {},
        guidedQueryEditor: {},
    };

    this.guidedQuery;

    //
    function updateQueryInEditor(str){
        gqe.editor.setValue(str, 1)
    }
    ////////////////////////////Private functionality
    function initZoomDialog() {
        //TODO: Add onDispose

        $(document.getElementById(selectors.RESULTS_FORM)).appendTo($(selectors.DATATABLE_ZOOM));
        $("#dialogElement").css("left", "150px").css("right", "150px");
        $(".ui-datatable-scrollable-body").css("height", "400px");
    }

    function disposeZoomDialog() {
        $(document.getElementById(selectors.RESULTS_FORM)).appendTo($(selectors.RESULTS_BODY));
        $(".ui-datatable-scrollable-body").css("height", "160px");

        $(selectors.DATATABLE_ZOOM).empty();
    }

    function removeParameter(rowId) {
        rowId = "#" + rowId;

        var parameterOccurenceIndex = $(rowId).data("occurrence");
        var parameterName = $(rowId).data("parameter");
        removeParameterFromCurrentParameters(parameterOccurenceIndex, parameterName);
        $(rowId).remove();

        updateQueryPreview();
    }

    function removeParameterFromCurrentParameters(occ, name) {

        for (var i in guidedQuery.remainingParameters) {
            if (guidedQuery.remainingParameters[i].name == name &&
                guidedQuery.currentOccurrencyIndex[getParameterKey(guidedQuery.remainingParameters[i])].times == occ) {
                guidedQuery.remainingParameters[i].deleted = true;
                guidedQuery.currentOccurrencyIndex[getParameterKey(guidedQuery.remainingParameters[i])].deleted.push(occ);
                break;
            }
        }

    }

    function updateQueryPreview() {
        var splittedQuery = guidedQuery.originalQuery.split(" ");
        var indexToReplace = [];

        updateFunctionCallPreview();
    }

    function updateFunctionCallPreview() {
        guidedQuery.queryName = $(selectors.GUIDED_DIALOG.GQ_NAME).val();
        var hadAtLeastOneParam = false;

        var $fixture =
            "<code class='fade-success'><span class='underline'>" + guidedQuery.queryName + "</span>(";

        for (var i in guidedQuery.remainingParameters) {
            if (!guidedQuery.remainingParameters[i].deleted) {
                $fixture += ("<kbd>" + guidedQuery.remainingParameters[i].value + "</kbd>,");
                hadAtLeastOneParam = true;
            }
        }

        if (hadAtLeastOneParam) {
            $fixture = $fixture.slice(0, -1);
        }
        $fixture += ")</code>";

        $(selectors.GUIDED_DIALOG.PREVIEW_CALL).empty();
        $($fixture).appendTo(selectors.GUIDED_DIALOG.PREVIEW_CALL);
    }

    //TODO
    function smartReplace(i) {
        var totalReplaced = guidedQuery.currentOccurrencyIndex[getParameterKey(guidedQuery.remainingParameters[i])].deleted;
        var stringToSearch = guidedQuery.remainingParameters[i].value;
        var originalIndex = guidedQuery.remainingParameters[i].occurrenceIndex;

        var count = 0;
        var pos = guidedQuery.currentQuery.indexOf(stringToSearch);

        while (pos !== -1) {
            count++;
            pos = str.indexOf('e', pos + 1);
        }
    }

    function reparseQuery() {
        loadQueryParameters(gqe.guidedQueryEditor);
        updateQueryPreview();
    }

    function init() {
        $(selectors.HORIZONTAL_TOGGLER).bind('click', toggleRightComponent);

        gqe.editor =
            $(selectors.EDITOR).length > 0 ? createAndInitAceEditor(selectors.EDITOR.substring(1)) : {};

        //############### Bind Event handlers
        gqe.editor.getSession().on('change', function (e) {
            document.getElementById(selectors.CURRENT_QUERY).value = getQueryToRun(gqe.editor);
        });

        document.getElementById(selectors.CURRENT_QUERY).value = getQueryToRun(gqe.editor);
    }

    function handleMessage(facesmessage) {
        facesmessage.severity = 'info';
        switch (facesmessage.detail[0])
        {
            case "E":
                facesmessage.severity="error";
                break;
            case "I":
                facesmessage.severity="info";
                break;
        }

        facesmessage.detail = facesmessage.detail.substring(1);
        PF('growl').show([facesmessage]);
    }

    function createAndInitAceEditor(elementId) {
        guidedQuery = {
            originalQuery: "",
            currentQuery: "",
            currentOccurrencyIndex: {},
            remainingParameters: {},
        };

        var editor = ace.edit(elementId);
        editor.setTheme(dashboardState.ace_theme);
        editor.getSession().setMode(dashboardState.ace_editor_mode);
        editor.setFontSize(14);
        return editor;
    }

    function initGuidedDialog() {
        gqe.guidedQueryEditor = createAndInitAceEditor(selectors.GUIDED_DIALOG.EDITOR.substring(1))
        gqe.guidedQueryEditor.setValue(getQueryToRun(gqe.editor), 1);


        loadQueryParameters(gqe.guidedQueryEditor);
        updateQueryPreview();
    }

    function loadQueryParameters() {
        guidedQuery.originalQuery = getQueryToRun(gqe.guidedQueryEditor);
        var parsedQuery = parseQuery(guidedQuery.originalQuery);

        var parsedParameters = [];

        parsedQuery.hasOwnProperty("SELECT") ?
            parsedParameters = parsedParameters.concat(getSelectParameters(parsedQuery["SELECT"])) : null;
        parsedQuery.hasOwnProperty("FROM") ?
            parsedParameters = parsedParameters.concat(getFromParameters(parsedQuery["FROM"])) : null;
        parsedQuery.hasOwnProperty("WHERE") ?
            parsedParameters = parsedParameters.concat(getWhereParameters(parsedQuery["WHERE"])) : null;
        parsedQuery.hasOwnProperty("ORDER BY") ?
            parsedParameters = parsedParameters.concat(getOrderByParameters(parsedQuery["ORDER BY"])) : null;

        //adding occurrence index for each parameter
        parsedParameters = addOccurrenceToEachParameter(parsedParameters);
        guidedQuery.remainingParameters = parsedParameters;

        var $fixture = constructParameterHtmlNodes(parsedParameters);
        $(selectors.GUIDED_DIALOG.TABLE_BODY).empty();

        $(selectors.GUIDED_DIALOG.TABLE_BODY).append($fixture);
    }

    function constructParameterHtmlNodes(parsedParameters) {
        var $fixture = [];

        for (var i in parsedParameters) {
            $fixture[i] = [
                "<tr id=row_" + i + " data-occurrence=" + parsedParameters[i].occurrenceIndex.times + " data-parameter=" + parsedParameters[i].name + ">",
                "   <th scope='row'>" + parsedParameters[i].identifier + "</th>",
                "   <td>" + parsedParameters[i].name + "</td>",
                "   <td>" + parsedParameters[i].value + "</td>",
                "   <td>",
                '      <button class="btn btn-danger btn-sm" onclick="__gqe__.removeGuidedParameter(\'row_' + i + '\')" title="Remove Parameter">',
                "          <i class='fa fa-times'/>",
                "      </button>",
                "  </td>",
                "</tr>"
            ].join("\n");
        }

        return $fixture.join('\n');
    }

    function parseQuery(queryString) {
        return simpleSqlParser.sql2ast(queryString);
    }

    /*Adding occurence index for each parameter, in order to faster determine it's place in original query
     * Unique identifiers will have index 1
     * */

    function addOccurrenceToEachParameter(parsedParameters) {

        var occurrenceMap = {};

        for (var i in parsedParameters) {
            if (occurrenceMap.hasOwnProperty(getParameterKey(parsedParameters[i]))) {
                occurrenceMap[getParameterKey(parsedParameters[i])].times++;
            }
            else {
                occurrenceMap[getParameterKey(parsedParameters[i])] = {
                    times: 1,
                    deleted: []
                };
            }
        }

        for (var i in parsedParameters) {
            parsedParameters[i].occurrenceIndex = occurrenceMap[getParameterKey(parsedParameters[i])]
        }

        guidedQuery.currentOccurrencyIndex = occurrenceMap;
        return parsedParameters;

    }

    function getParameterKey(parameter) {
        if (parameter.identifier == "WHERE") {
            return parameter.name;
        }
        return parameter.value;
    }

    function disposeGuidedDialog() {
        //guidedQuery = {};
        gqe.guidedQueryEditor = {};
    }

    // hides results component
    // set's Editor height to max
    function toggleResultsComponent() {
        $(document.getElementById(selectors.RESULTS_FORM)).toggle();
        $(selectors.RESULTS).toggleClass('h-toggleable');
        $(selectors.EDITOR_CONTAINER).toggleClass('container-top');
        $(selectors.EDITOR).toggleClass('height-88');


        dashboardState.hiddenResultsContent = !dashboardState.hiddenResultsContent;

        //Focus Ace Editor
        gqe.editor.focus();

        var session = gqe.editor.getSession();
        //Get the number of lines
        var count = session.getLength();
        //Go to end of the last line
        gqe.editor.gotoLine(count, session.getLine(count - 1).length);
    }

    function toggleRightComponent() {
        $(selectors.CONTENT_CENTER).toggleClass('ui-grid-col-10');
        $(selectors.CONTENT_RIGHT).toggleClass('ui-grid-col-2');
        $(selectors.CONTENT_RIGHT).toggleClass("pullLeft");

        dashboardState.hiddenRightContent ? $(selectors.SAVE_QUERY_BUTTON).css("margin-right", "0") :
            $(selectors.SAVE_QUERY_BUTTON).css("margin-right", "4.5%");

        dashboardState.hiddenRightContent ? $("#contentRight > .ui-panel").css("height", "100%") :
            $("#contentRight > .ui-panel").css("height", "auto");

        dashboardState.hiddenRightContent = !dashboardState.hiddenRightContent;
    }

    function getQueryToRun(editor) {

        var selectedText = editor.session.getTextRange(editor.getSelectionRange());

        // check if user has selected text
        if (selectedText.length > 0) {
            //nothing, selectedText already contains desired value
        }
        else {
            selectedText = editor.getValue();
            // check if ace.editor contains any text
        }
        return selectedText;
    }

    function toggleGuidedQueryDialog() {
        PF(selectors.GUIDED_DIALOG.DIALOG).show();
    }

    function toggleZoomDialog() {
        PF(selectors.DATATABLE_ZOOM_DIALOG).show();
    }


    ////////// Parse params
    function getSelectParameters(select) {
        var columns = [];
        for (var i in select) {
            columns.push({
                identifier: "SELECT",
                name: select[i].name == '*' ? "select_all" : select[i].name,
                value: select[i].name,

            });
        }

        return columns;
    }

    function getFromParameters(from) {

        var tables = [];
        for (var i in from) {

            tables.push(
                {
                    identifier: "FROM",
                    name: "[table]_" + from[i].table,
                    value: from[i].table
                });
        }
        return tables;
    }

    function getWhereParameters(where) {

        var params = [];

        if (where.hasOwnProperty("terms")) {
            traverseNestedWhereExpression(where.terms, params);
        }
        else if (typeof where == "object") {
            //where contains only a condition
            traverseNestedWhereExpression([where], params);
        }

        return params;
    }

    function traverseNestedWhereExpression(terms, whereParams) {
        for (var term in terms) {
            switch (typeof terms[term]) {

                // A check against a boolean expression
                case "string":
                    whereParams.push(
                        {
                            identifier: "WHERE",
                            name: terms[term],
                            value: terms[term],
                        });
                    break;

                //we either found an terminal "terms", either a nested one(nested expression)
                case "object":
                {
                    //we found a nested where expression, traverse it in depth
                    if (terms[term].hasOwnProperty("terms")) {
                        traverseNestedWhereExpression(terms[term].terms, whereParams)
                    }
                    // we are in a terminal expression node. Get Operands
                    else if (terms[term].hasOwnProperty("operator")) {
                        whereParams.push(
                            {
                                identifier: "WHERE",
                                name: terms[term].left,
                                value: terms[term].right,
                            });
                    }
                }
            }


        }
    }

    function getOrderByParameters(orderBy) {
        var params = [];
        for (var i in orderBy) {

            params.push({
                identifier: "ORDER BY",
                name: orderBy[i].column,
                value: orderBy[i].order
            });
        }
        return params;
    }

    /* TODO: decide if it is worth implementing JOIN
     function getJoinParameters(join) {

     var params = [];
     for (var i in join) {
     if (!i.hasOwnProperty(i)) {
     continue;
     }
     params.push({
     identifier:"JOIN",
     type : join[i].name,
     });
     }
     }
     */


    function saveQueryProcess(event){
        PF('growl').renderMessage({"summary":"Guided Query",
            "detail":"Query Successfully submitted",
            "severity":"info"})

        debugger;
    }

    function checkForGuided(event){
        var queryStr = getQueryToRun(gqe.editor);
        var gqName = queryStr.substr(0,queryStr.indexOf("("));

        if(gqName.length>0 && gqName.length<30){
            if (guidedQuery.queryName == gqName)
            {
                var params = queryStr.substr(queryStr.indexOf("(")+1,queryStr.indexOf(")")+1).split(",");

                var originalQuery = guidedQuery.originalQuery;
                originalQuery.replace(100,params[0]);
                originalQuery.replace("C%",params[1]);

                document.getElementById(selectors.CURRENT_QUERY).value = originalQuery;
                // parse params, & replace query
            }
        }
        console.warn(queryStr);



    }

    //////////Attach to window + init()

    if (typeof window.__gqe__ === "undefined") {
        window.__gqe__ = gqe;
    }

    $(document).ready(gqe.init);


})();

