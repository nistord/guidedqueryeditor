package guidedQueryEditor.views.dashboard;

import guidedQueryEditor.Login.UserConnectionDetails;
import guidedQueryEditor.Database.Query.QueryManager;
import guidedQueryEditor.views.dashboard.content.databaseTree.TreeData;
import org.apache.commons.lang.StringEscapeUtils;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import java.io.IOException;
import java.io.Serializable;
import java.util.ResourceBundle;

/**
 * Created by daniel on 06.05.2016.
 */

/**
 * @Class that handles all user interactions with Dashboard
 * It's initialised as soon as user login's.
 * It does a check, to make sure that user is already logged in,
 * to prevent unauthenticated access.
 */
@ManagedBean(eager = true)
@ViewScoped
public class DashboardView implements Serializable {

    private static boolean firstVisit = true;
    private static String CHANNEL = "/notify";
    private transient ResourceBundle localeBundle;
    private static final String copyright = "Copyright © Daniel.";
    private String notificationSummary = "";
    private String notificationDetails = "";

    @ManagedProperty(value = "#{userConnectionDetails}")
    private UserConnectionDetails userConnectionDetails;

    /**
     * @Class QueryManager handles all sql queries: performed by user &
     * by dashboard(automatically, e.g table list, query exec details)
     */
    @ManagedProperty(value = "#{queryManager}")
    public QueryManager queryManager;

    TreeData databaseTreeData;

    @PostConstruct
    public void init() {

        localeBundle = ResourceBundle.getBundle("guidedQueryEditor.language.string", FacesContext.getCurrentInstance().getViewRoot().getLocale());

        if (userConnectionDetails.getSessionId() == null)
            userConnectionDetails.setSessionId(FacesContext.getCurrentInstance().getExternalContext().getSessionId(false));

        //redirect user
        if (!userConnectionDetails.isUserLoggedIn()) {
            try {
                // illegal access, redirect
                // similar to login, but it doesn't invalidate session
                ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
                ec.invalidateSession();
                ec.redirect(ec.getRequestContextPath() + "/login");
            } catch (IOException e) {
                e.printStackTrace();
                notificationSummary = "ESomething went wrong!";
                notificationDetails = "Check error tab to see what went wrong.";
            }
        }
        //welcome user
        else {
            //init Query Manager
            queryManager.getQueryBuilder().setConn(userConnectionDetails.getConnection());

            //init DatabaseTreeData
            databaseTreeData = new TreeData(userConnectionDetails.getConnection());

            //welcoming user when logs in
            if (firstVisit) {
                notificationSummary = "IWelcome " + userConnectionDetails.getUsername();
            }
            firstVisit = firstVisit ? false : false;
        }

        if (!notificationSummary.isEmpty()) {
            sendNotificationToClient(notificationDetails, notificationSummary);
            notificationSummary = "";
            notificationDetails = "";
        }
    }

    private void sendNotificationToClient(String summary, String details) {
        EventBus eventBus = EventBusFactory.getDefault().eventBus();
        eventBus.publish(CHANNEL, new FacesMessage(StringEscapeUtils.escapeHtml(summary), StringEscapeUtils.escapeHtml(details)));
    }

    public String getCopyright() {
        return copyright;
    }

    public void setUserConnectionDetails(UserConnectionDetails userConnectionDetails) {
        this.userConnectionDetails = userConnectionDetails;
    }

    public UserConnectionDetails getUserConnectionDetails() {
        return userConnectionDetails;
    }

    public QueryManager getQueryManager() {
        return queryManager;
    }

    public void setQueryManager(QueryManager queryManager) {
        this.queryManager = queryManager;
    }

    public TreeData getDatabaseTreeData() {
        return databaseTreeData;
    }

    public void setDatabaseTreeData(TreeData databaseTreeData) {
        this.databaseTreeData = databaseTreeData;
    }

    public String getNotificationSummary() {
        return notificationSummary;
    }

    public void setNotificationSummary(String notificationSummary) {
        this.notificationSummary = notificationSummary;
    }

    public String getNotificationDetails() {
        return notificationDetails;
    }

    public void setNotificationDetails(String notificationDetails) {
        this.notificationDetails = notificationDetails;
    }
}
