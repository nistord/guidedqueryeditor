package guidedQueryEditor.views.dashboard.content.databaseTree;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import java.util.*;
import java.util.Map.Entry;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * Created by daniel on 29.04.2016.
 */

public class TreeData implements Serializable {
    private TreeNode root;

    private Connection connection;

    public TreeData(){    }

    public TreeData(Connection conn){
        this.connection = conn;
        loadDatabaseSchemas();
    }

    private void loadDatabaseSchemas(){
        Map treeNodes = new HashMap<String,List<String>>();
        String[] TYPES={"TABLE"};

        try {
            DatabaseMetaData metaData = connection.getMetaData();

            ResultSet tables = metaData.getTables("", "", "", TYPES);
            while (tables.next()) {

                //if(tables.getString(2)==){
                //if this schema isn't in treeNodes, add it.
                if (!treeNodes.containsKey(tables.getString(2))) {
                    treeNodes.put(tables.getString(2), new LinkedList<>());
                }

                ((LinkedList) treeNodes.get(tables.getString(2))).add(tables.getString(3));
            //}
            }
        } catch (SQLException e) {
            Logger.getGlobal().warning(e.getMessage());
        }

        addNodesToRoot(treeNodes);
    }

    private void addNodesToRoot(Map treeNodes) {

        root = new DefaultTreeNode("Root",null);
        for (Entry<String, List<String>> pair : (Iterable<Entry<String, List<String>>>) treeNodes.entrySet()) {
            // adding schema node to root
            TreeNode schemaNode = new DefaultTreeNode("[Schema] " + pair.getKey(), root);
            schemaNode.setExpanded(true);

            //adding table node to schema
            for (String childNode : pair.getValue()) {
                TreeNode table = new DefaultTreeNode("[T] " + childNode, schemaNode);
            }

            //add other DB entities here: views, procedures, indexes, etc.
        }



    }

    public TreeNode getRoot(){
        //refresh();
        return root;
    }

    public void refresh(){
        loadDatabaseSchemas();
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

}
