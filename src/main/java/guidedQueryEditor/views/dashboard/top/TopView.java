package guidedQueryEditor.views.dashboard.top;

import guidedQueryEditor.Database.Connection.JDBCConnectionPool;
import guidedQueryEditor.Login.UserConnectionDetails;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import java.io.IOException;

/**
 * Created by daniel on 26.04.2016.
 */

@ManagedBean(eager = true)
@ViewScoped
public class TopView {


    @ManagedProperty(value="#{connectionsPool}")
    private JDBCConnectionPool connectionsPool;
    /**
     * This invalidates session, and redirects to login page
     * */
    public void logout(ActionEvent event) throws IOException {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();

        connectionsPool.removeInactiveUser(ec.getSessionId(false));
        ec.invalidateSession();
        ec.redirect(ec.getRequestContextPath() + "/login");
    }

    public JDBCConnectionPool getConnectionsPool() {
        return connectionsPool;
    }

    public void setConnectionsPool(JDBCConnectionPool connectionsPool) {
        this.connectionsPool = connectionsPool;
    }
}
