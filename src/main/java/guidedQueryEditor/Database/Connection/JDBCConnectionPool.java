package guidedQueryEditor.Database.Connection;

import guidedQueryEditor.Login.UserConnectionDetails;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

@ManagedBean(name="connectionsPool",eager = true)
@ApplicationScoped
public class JDBCConnectionPool extends ObjectPool<UserConnectionDetails> {

    private final String DEFAULT_DRIVER = "org.postgresql.Driver";
    private final String ORACLE_DRIVER = "oracle.jdbc.driver.OracleDriver";

    private String sessionIdOfCurrentRequest = null;
    private final Object lock = new Object();

    // when users log's in an entry will be added here with his details
    private Map<String, UserConnectionDetails> connectionDetailsForActiveUsers;


    public JDBCConnectionPool() {
        super();

        DriverManager.setLoginTimeout(40);
        connectionDetailsForActiveUsers = new ConcurrentHashMap<String, UserConnectionDetails>();

        //Instantiating default driver at Startup
        try {
            Class.forName(DEFAULT_DRIVER).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *  Function accessed by ConnectionDetails Bean (@Session scope)
     *  It returns a connection wrapped withing an object @return Connection Details
     * */
    @Override
    protected UserConnectionDetails create(UserConnectionDetails userConnectionDetails) {

        try {
            synchronized (lock) {
                // Identify which user is requesting a connection
                sessionIdOfCurrentRequest = userConnectionDetails.getSessionId();
                userConnectionDetails.setConnection(
                        DriverManager.getConnection(userConnectionDetails.getJdbcUrl(),
                                userConnectionDetails.getUsername(),
                                userConnectionDetails.getPassword())
                );

                //add connection to ActiveUsers
                Logger.getGlobal().info("[Conn-POOL]:[" + userConnectionDetails.getUsername()
                        + ":ssid:" + userConnectionDetails.getSessionId() +"] CONNECTED to ["
                        + userConnectionDetails.getDatabaseName()+"] database!");
                connectionDetailsForActiveUsers.put(sessionIdOfCurrentRequest, userConnectionDetails);

                //connection was successful, clear previous exception if exists
                userConnectionDetails.clearException();
            }
            return userConnectionDetails;

        } catch (SQLException e) {
            userConnectionDetails.setException(e);
            e.printStackTrace();
            return (null);
        }
    }

    @Override
    public void expire(UserConnectionDetails connDetails) {

        try {
            (connDetails.getConnection()).close();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            Logger.getGlobal().info("[Conn-POOL]:[" + connDetails.getUsername() + "] DISCONNECTED from ["+connDetails.getDatabaseName()+"] database!");

        }
    }

    @Override
    public void update(UserConnectionDetails o) {
        this.sessionIdOfCurrentRequest = o.getSessionId();
    }

    @Override
    public boolean validate(UserConnectionDetails connDetails) throws SQLException {
        return (connDetails.getSessionId().equals(sessionIdOfCurrentRequest) &&
                !connDetails.getConnection().isClosed());
    }

    /**
     *  Returns the driver used to init all connections
     *  for current user. Based on ConnectionDetails and App Cookie
     * @return a string representing JDBC driver used to open connection
     */
    public String getDriver(){

        String driver = "";

        switch ("Cookie param - set on login page"){
            case "POSTGRE":
                driver = DEFAULT_DRIVER;
                break;
            case "ORACLE":
                driver = ORACLE_DRIVER;
                break;
            default:
                driver = DEFAULT_DRIVER;

        }
        return  driver;
    }

    /**
     * Invoked from ConnectionDetails when a session gets terminated
     * @param sessionId - Id of expired session
     */
    public void removeInactiveUser(String sessionId){

        UserConnectionDetails inactiveUser = connectionDetailsForActiveUsers.get(sessionId);
        if(inactiveUser != null){
            try{
                expire(inactiveUser);
            }
            catch (Exception e) {
                Logger.getGlobal().warning("Could not close conn for #ssid: " + sessionId
                        + ". Connection probably closed.");
                e.printStackTrace();
            }
            finally {
                Logger.getGlobal().info("[Conn-POOL]:[Removed INACTIVE user][ssid]:" + sessionId);
                connectionDetailsForActiveUsers.remove(sessionId);
            }
        }

    }

    public boolean checkIfUserIsLoggedIn(String SSID){
        UserConnectionDetails user = connectionDetailsForActiveUsers.get(SSID);
        // if user is logged in, means that he successfully connected to a db
        // through create()
        return user != null;
    }
}

