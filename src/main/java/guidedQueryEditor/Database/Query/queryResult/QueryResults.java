package guidedQueryEditor.Database.Query.queryResult;

import guidedQueryEditor.Database.Query.queryResult.queryInfo.QueryInfo;
import sun.rmi.runtime.Log;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by daniel on 29.04.2016.
 */

/*
* Done:
*   -
* TODO:
* */
public class QueryResults implements Serializable {

    private QueryInfo queryInfo;
    private List<ColumnModel> columns;
    private List<Map<String, String>> returnedRows;

    private Map<String, String> selectedRow;

    private ParsedSqlException exception;
    private boolean hasException = false;


    public QueryResults() {
        exception = new ParsedSqlException();
        queryInfo = new QueryInfo();
        queryInfo.execTime = 0;
        queryInfo.totalResults = 0;
        queryInfo.rowsAffected = 0;
        queryInfo.queryString = "No query was run!";

    }

    public void executedUpdate(PreparedStatement result) {
        clearStats();
        //TODO: queryInfo.setAffectedRows(?)
    }

    public void executedQuery(PreparedStatement result) {
        clearStats();

        try {
            parseMetadata(result.getMetaData());
            parseResults(result.getResultSet());
        } catch (SQLException e) {
            updateException(e);
            Logger.getGlobal().warning(e.getMessage());
            // TODO: Growl
        }
        Logger.getGlobal().info("Successfully executed query. Rows: " + queryInfo.getTotalResults());
    }

    //Extracts column names/label from result metadata
    private void parseMetadata(ResultSetMetaData metadata) throws SQLException {
        String[] columnNames = new String[metadata.getColumnCount()];

        //column indexes in ResultSetMetaData start from 1
        for (int i = 0; i < metadata.getColumnCount(); i++) {
            columnNames[i] = metadata.getColumnName(i + 1).length() > 0
                    ? metadata.getColumnName(i + 1) : metadata.getColumnLabel(i + 1);
        }

        createDynamicColumns(columnNames);
    }

    private void createDynamicColumns(String[] colsParsedFromMetadata) {
        columns = new ArrayList<ColumnModel>();

        for (String columnKey : colsParsedFromMetadata) {
            columns.add(new ColumnModel(columnKey.toUpperCase(), columnKey));
        }
    }

    private void parseResults(ResultSet resultSet) throws SQLException {
        int totalRows = 0;
        returnedRows = new ArrayList<Map<String, String>>();
        //
        while (resultSet.next()) {
            Map temporaryRow = new HashMap<String, String>(columns.size());

            for (int i = 0; i < columns.size(); i++) {
                // column indexes & row values in ResultSetMetaData start from 1
                temporaryRow.put(columns.get(i).getHeader(),
                        String.valueOf(resultSet.getObject(i + 1)));

            }
            // TODO:
            //    - extract all data from rows based on col Id/Name from Metadata
            //    - find a solution for getXXX / or implement an Factory/Parser(row,colType) -> string?

            returnedRows.add(temporaryRow);
            totalRows++;
        }
        queryInfo.setTotalResults(totalRows);
    }

    public void updateException(SQLException exception) {
        hasException = true;
        this.exception.parseException(exception);
    }

    private void clearStats() {
        queryInfo.execTime = 0.0;
        queryInfo.totalResults = 0;
        queryInfo.rowsAffected = 0;
        queryInfo.queryString = "Nothing here yet.";

        clearException();
        //TODO: clear columns + rows. Test to see how it looks.
    }

    private void clearException() {
        hasException = false;
        exception.clear();
    }


    // Getters and Setters
    public QueryInfo getQueryInfo() {
        return queryInfo;
    }

    public void setQueryInfo(QueryInfo queryInfo) {
        this.queryInfo = queryInfo;
    }

    public List<ColumnModel> getColumns() {
        return columns;
    }

    public List<Map<String, String>> getReturnedRows() {
        return returnedRows;
    }

    public void setReturnedRows(List<Map<String, String>> returnedRows) {
        this.returnedRows = returnedRows;
    }

    public Map<String, String> getSelectedRow() {
        return selectedRow;
    }

    public void setSelectedRow(Map<String, String> selectedRow) {
        this.selectedRow = selectedRow;
    }

    static public class ColumnModel implements Serializable {

        private String header;
        private String property;

        public ColumnModel(String header, String property) {
            this.header = header;
            this.property = property;
        }

        public String getHeader() {
            return header;
        }

        public String getProperty() {
            return property;
        }
    }

    static public class ParsedSqlException {


        private String exceptionSummary;
        private String[] exceptionDetails;

        public void parseException(SQLException e) {

            String[] parsedError = e.getMessage().split("\n");
            exceptionSummary = "Error code : " + e.getSQLState() + " " + parsedError[0];
            exceptionDetails = parsedError;
        }

        public void clear(){
            exceptionSummary = null;
            exceptionDetails = null;
        }

        public String getExceptionSummary() {
            return exceptionSummary;
        }

        public void setExceptionSummary(String exceptionSummary) {
            this.exceptionSummary = exceptionSummary;
        }

        public String[] getExceptionDetails() {
            return exceptionDetails;
        }

        public void setExceptionDetails(String[] exceptionDetails) {
            this.exceptionDetails = exceptionDetails;
        }

    }

    public ParsedSqlException getException() {
        return exception;
    }

    public void setException(ParsedSqlException exception) {
        this.exception = exception;
    }
}
