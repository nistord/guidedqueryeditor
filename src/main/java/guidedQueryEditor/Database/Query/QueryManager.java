package guidedQueryEditor.Database.Query;

import guidedQueryEditor.Database.Query.queryResult.QueryResults;
import sun.rmi.runtime.Log;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * Created by daniel on 24.05.2016.
 */

@ManagedBean(eager = true)
@ViewScoped
public class QueryManager {

    private String currentQuery;

    private QueryBuilder queryBuilder;

    private QueryResults queryResults;

    public QueryManager(){
        queryBuilder = new QueryBuilder();
        queryResults = new QueryResults();
    }

    public void init() {
        queryBuilder = new QueryBuilder();
        queryResults = new QueryResults();
    }

    public void evaluateQuery(ActionEvent event) throws IOException {

        /* TODO //QB: parse(check for guided && UPDATE) -> build(params?)
        Properties properties = new Properties();
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        properties.load(ec.getResourceAsStream("/WEB-INF/sqlQueries/queries.json"));

        Logger.getGlobal().info(properties.getProperty("query.multipleRows"));*/

        if (currentQuery != null && currentQuery.trim().length() > 0) {

            Logger.getGlobal().info("Executing : \"" + currentQuery + '\"');
            executeQuery(currentQuery, false);
        } else {
            // TODO: 'growl' e.g No query provided
            Logger.getGlobal().warning("Empty Query!");
        }
    }

    // Parser checks if query is an UPDATE query;
    private void executeQuery(String currentQuery, boolean isUpdate) {

        PreparedStatement inputQuery;

        try {
            inputQuery = queryBuilder.buildQueryFromString(currentQuery);

            //exec time statistics
            long start = System.nanoTime();
            inputQuery.execute();
            long end = System.nanoTime();
            queryResults.getQueryInfo().setExecTime((end - start)/1000000);

            if(isUpdate){
                queryResults.executedUpdate(inputQuery);
            }
            else{
                queryResults.executedQuery(inputQuery);
            }
            //TODO: Add Execute Batch?
            queryResults.getQueryInfo().setQueryString(currentQuery);

            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Successful",  "Query was run in "+queryResults.getQueryInfo().getExecTime()+" ms.") );

        } catch (SQLException exception) {

            // Log query error message
            Logger.getGlobal().warning(exception.getMessage());
            queryResults.updateException(exception);

            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error",  "Check error tab for further details.") );
        }

    }


    // Getters and Setters
    public QueryResults getQueryResults() {
        return queryResults;
    }

    public void setQueryResults(QueryResults queryResults) {
        this.queryResults = queryResults;
    }

    public QueryBuilder getQueryBuilder() {
        return queryBuilder;
    }

    public void setQueryBuilder(QueryBuilder queryBuilder) {
        this.queryBuilder = queryBuilder;
    }

    public String getCurrentQuery() {
        return currentQuery;
    }

    public void setCurrentQuery(String currentQuery) {
        this.currentQuery = currentQuery;
    }
}
