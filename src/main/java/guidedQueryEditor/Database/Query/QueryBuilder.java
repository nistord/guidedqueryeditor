package guidedQueryEditor.Database.Query;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by daniel on 24.05.2016.
 */
public class QueryBuilder {

    private Connection connection;

    public QueryBuilder setConn(Connection conn){
        this.connection = conn;
        return this;
    }

    public PreparedStatement buildQueryFromString(String query) throws SQLException {
        PreparedStatement result = connection.prepareStatement(query);
        return result;
    }
}
