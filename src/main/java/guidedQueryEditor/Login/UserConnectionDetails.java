package guidedQueryEditor.Login;


import guidedQueryEditor.Database.Connection.JDBCConnectionPool;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by daniel on 03.05.2016.
 */

@ManagedBean(eager = true)
@SessionScoped
public class UserConnectionDetails implements HttpSessionBindingListener, Serializable {

    private String jdbcUrl;
    private String databaseName;

    private String databaseHost = "pagila.c1gude4pd8xo.us-west-2.rds.amazonaws.com";

    private String username;
    private String password;
    private String sessionId;

    private String jdbcDriver;
    private Connection connection;
    private SQLException exception;
    private boolean hasException = false;

    /**
     * Used to handle multiple logins from same user.
     * When user log's in twice, first session is invalidated
     * */
    private static Map<UserConnectionDetails, HttpSession> logins = new ConcurrentHashMap<>();

    @ManagedProperty(value="#{connectionsPool}")
    private JDBCConnectionPool connectionPool;


    public boolean isUserLoggedIn(){
        return connectionPool.checkIfUserIsLoggedIn(sessionId);
    }

    public void connectDefault(ActionEvent event) {

        this.jdbcDriver = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap().get("jdbcDriver");

        jdbcUrl = constructJdbcUrl(jdbcDriver,databaseName,databaseHost);

        connectToDatabase();

        // User logged in: redirect to dashboard page
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("/dashboard/standard");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void connectToDatabase() {
        this.sessionId = FacesContext.getCurrentInstance().getExternalContext().getSessionId(false);
        try {
            this.connection = connectionPool.checkOut(this).getConnection();
        } catch (SQLException e) {

            //notify client
            e.printStackTrace();
        }

        //return the connection, user successfully logged in
        connectionPool.checkIn(this);
    }

    public void setConnectionPool(JDBCConnectionPool connP){
        this.connectionPool = connP;
    }

    public String getJdbcDriver() {
        return jdbcDriver;
    }

    public void setJdbcDriver(String jdbcDriver) {
        this.jdbcDriver = jdbcDriver;
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDatabaseHost() {
        return databaseHost;
    }

    public void setDatabaseHost(String databaseHost) {
        this.databaseHost = databaseHost;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void valueBound(HttpSessionBindingEvent event) {
        HttpSession session = logins.remove(this);
        if (session != null) {
            connectionPool.removeInactiveUser(session.getId());
            session.invalidate();
        }
        logins.put(this, event.getSession());
    }

    @Override
    public void valueUnbound(HttpSessionBindingEvent event) {
        HttpSession session = logins.remove(this);
        if (session != null) {
            connectionPool.removeInactiveUser(session.getId());
        }
    }

    @Override
    public boolean equals(Object other) {
        return (other instanceof UserConnectionDetails) && (username != null) ? username.equals(((UserConnectionDetails) other).username) : (other == this);
    }

    @Override
    public int hashCode() {
        return (username != null) ? (this.getClass().hashCode() + username.hashCode()) : super.hashCode();
    }

    private String constructJdbcUrl(String driver,String databaseName, String host){

        return "jdbc:"+driver+"://"+host+"/"+databaseName;
    }

    public void setException(SQLException exception) {
        hasException = true;
        this.exception = exception;
    }
    public void clearException(){
        hasException = false;
        exception = null;
    }
}
